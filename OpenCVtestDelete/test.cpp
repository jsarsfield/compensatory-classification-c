#include "test.h"
#include <opencv2/ximgproc.hpp>
#include "opencv2/highgui.hpp"
#include "opencv2/core/utility.hpp"

using namespace cv;
using namespace cv::ximgproc;

std::string modelFilename = "model.yml.gz";
cv::Ptr<StructuredEdgeDetection> pDollar = createStructuredEdgeDetection(modelFilename);

void __stdcall DrawImage(unsigned char* imageData, int size, int width, int height) {
	std::string inFilename = "edgeDetectionTest1.png";
	std::string outFilename = "";//edgeDetectionTest1Depth.png

	cv::Mat image = cv::Mat(height, width, CV_8UC4, imageData); //cv::imdecode(*imageData, cv::ImreadModes::IMREAD_COLOR); //cv::imread(inFilename, 1);

	/*if (image.empty())
	{
		printf("Cannot read image file: %s\n", inFilename.c_str());
		return;
	}*/
	image.reshape(3).convertTo(image, CV_BGRA2BGR565);
	image.convertTo(image, cv::DataType<float>::type, 1 / 255.0);

	cv::Mat imageL = cv::imread(inFilename, 1);
	cv::Mat edges(image.size(), image.type());
	pDollar->detectEdges(image, edges);

	if (outFilename == "")
	{
		cv::namedWindow("edges", 1);
		cv::imshow("edges", edges);
		cv::waitKey(0);
	}
	//else
	//	cv::imwrite(outFilename, 255 * edges);
}
