﻿using UnityEngine;
using System;
using System.IO.Pipes;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;

public class video : MonoBehaviour {

    // http://docs.unity3d.com/ScriptReference/Texture2D.LoadRawTextureData.html
    // byte array to texture

    private AnonymousPipeClientStream pipeClient;
    public joints jointsScript; // Ref to joints script object for passing position data
    public camera camScript; // Ref to cam script
    //private BinaryReader sr;
    private BinaryFormatter formatter = new BinaryFormatter();
    public float kinectWidth = 1920f;
    public float kinectHeight = 1080f;
    public float camWidth;  // Width of video in screen pixels 
    public float camHeight; // Height of video in screen pixels
    public float camX;  // Horizontal start of video in screen pixels
    public float camY; // Vertical start of video in screen pixels
    public float ratioX; // ratio between kinect pixels & cam pixels to scale kinect joint data
    public float ratioY;
    MeshRenderer sr;
    Texture2D tex; // Texture to update with Kinect pixel data
    byte[] imgData;
    int byteLen = (1920 * 1080) * 4;


    private void Start()
    {
        sr = GetComponent<MeshRenderer>();
        sr.material.SetTextureScale("_MainTex", new Vector2(-1, 1));
        tex = new Texture2D(1920, 1080, TextureFormat.RGBA32, false);
        imgData = new byte[byteLen];

        for (int i = 0; i < byteLen; i++)
        {
            if ((i +1) % 4 == 0)
            {
                imgData[i] = 255;
            }
            else
            {
                imgData[i] = 255;
            }
        }
        tex.LoadRawTextureData(imgData);
        tex.Apply();
        GetComponent<Renderer>().material.mainTexture = tex;

        camScript.AdjustAspectRatio();

        Debug.Log("start");
        OpenPipe();
        Debug.Log("end");
    }

    public void ResizeSpriteToScreen(Rect cameraRect)
    {
        camWidth = Screen.width * cameraRect.width;
        camHeight = Screen.height * cameraRect.height;
        camX = Screen.width * cameraRect.x;
        camY = Screen.height * (cameraRect.y+cameraRect.height); // y+height because Kinect and Unity Y axis differ
        ratioX = camWidth / kinectWidth;
        ratioY = camHeight / -kinectHeight;

        transform.localScale = new Vector3(1, 1, 1);

        float width = sr.bounds.size.x;
        float height = sr.bounds.size.y;
        int screenW = (int)(Screen.width * cameraRect.width); // Convert screen width to camera bounds
        int screenH = (int)(Screen.height * cameraRect.height); // Convert screen height to camera bounds

        float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
        float worldScreenWidth = worldScreenHeight / screenH * screenW;

        transform.localScale = new Vector3(worldScreenWidth / width, worldScreenHeight / height, 1);
        //jointsScript.UpdateJointPositions(null);
    }

    // Convert Kinect position to unity position
    public Vector3 KinectToUnityPoint(float kinectX, float kinectY)
    {
        return Camera.main.ScreenToWorldPoint(new Vector3(camX + (kinectX * ratioX), camY + (kinectY * ratioY), 10));
    }

    // When we receive byte array of a new frame update the texture of this video sprite
    public void UpdateVideoFrame(byte[] pixels)
    {
        tex.LoadRawTextureData(pixels);
        tex.Apply();
        GetComponent<Renderer>().material.mainTexture = tex;
    }

    // Open pipe and capture incoming data.
    private void OpenPipe()
    {
        new Thread(delegate ()
        {
            string pipeHandle = System.Environment.GetCommandLineArgs()[1].ToString();
            pipeClient = new AnonymousPipeClientStream(PipeDirection.In, pipeHandle);
            while (true)
            {
                Dictionary<string,object> data = (Dictionary<string, object>)formatter.Deserialize(pipeClient);
                if (data != null)
                {
                    // If list<double> then is joint data
                    if (data.ContainsKey("joints"))
                    {
                        lock (ThreadedVariables.Instance.joints)
                        {
                            ThreadedVariables.Instance.joints = Extensions.Clone((List<double[]>)data["joints"]);
                        }
                        Dispatcher.Instance.Invoke(() => jointsScript.UpdateJointPositions());
                    }
                    else if (data.ContainsKey("angles"))
                    {
                        Dispatcher.Instance.Invoke(() => jointsScript.UpdateAngles((List<float>)data["angles"]));
                    }
                    // Otherwise it is byte[] data for the video stream
                    else if (data.ContainsKey("video"))
                    {
                        byte[] videoData = (byte[])data["video"];
                        byte[] frame = new byte[videoData.Length];
                        Array.Copy(videoData, 0, frame, 0, videoData.Length);
                        Dispatcher.Instance.Invoke(() => UpdateVideoFrame(frame));
                    }
                }
                else
                {
                    Thread.Sleep(10);
                }  
            }
        }).Start();
    }

    // Update is called once per frame
    void Update () {

    }
}


