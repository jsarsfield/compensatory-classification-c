﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class joints : MonoBehaviour {

    public Object prefab;   // Prefab to instantiate joint objects from
    public video vidScript; // Ref to video script object
    int numberOfJoints = 13;
    List<string> jointNames = new List<string>() { "SpineBase", "SpineMid", "Neck", "Head", "ShoulderLeft", "ElbowLeft", "WristLeft", "ShoulderRight", "ElbowRight", "WristRight", "HipLeft", "HipRight", "SpineShoulder" };
    List<GameObject> jointsRefs = new List<GameObject>(); // References to joint objects instantiated
    List<GameObject> angleGameObjects = new List<GameObject>(); // Game objects of angles
    Vector2 forward = new Vector2(1, 0); // Forward vector for drawing angle

    // Use this for initialization
    void Start () {
	    for(int i = 0; i < numberOfJoints; i++)
        {
            GameObject joint = (GameObject)Instantiate(prefab);
            joint.transform.parent = this.transform;
            joint.name = jointNames[i];
            joint.GetComponent<SpriteRenderer>().sortingLayerName = "Foreground";
            jointsRefs.Add(joint);
        }
    }

    // Update joint positions when we receive data from pipe
    public void UpdateJointPositions()
    {
        lock (ThreadedVariables.Instance.joints)
        {
            if (ThreadedVariables.Instance.joints.Count == jointNames.Count)
            {
                for (int i = 0; i < numberOfJoints; i++)
                {
                    if (ThreadedVariables.Instance.joints[i].Length == 2)
                        jointsRefs[i].transform.position = vidScript.KinectToUnityPoint((float)ThreadedVariables.Instance.joints[i][0], (float)ThreadedVariables.Instance.joints[i][1]);
                }
            }
            // Draw angle around elbow TESTING
            //UpdateAngles(ThreadedVariables.Instance.angles[0]);
        }
    }
    // Temp function for visualising angles between joints
    public void UpdateAngles(List<float> angles)
    {
        for (int i = 0; i < angles.Count; i++)
        {
            Vector2 angleVec = Extensions.DegreeToVector2(angles[i]);
            //start = start.normalized;
            //end = end.normalized;
            if (angleGameObjects.Count != angles.Count)
            {
                angleGameObjects.Add(new GameObject());
                angleGameObjects[i].transform.parent = this.transform;
                angleGameObjects[i].AddComponent<MeshRenderer>();
                MeshFilter filter = angleGameObjects[i].AddComponent<MeshFilter>();
                filter.mesh = new Mesh();
                Vector3 v = filter.transform.position;
                v.z = -1;
                filter.transform.position = v;
                Color col = angleGameObjects[i].GetComponent<Renderer>().material.color;
                col.a = 255;
                col.r = 255;
                col.g = 0;
                col.b = 0;
                angleGameObjects[i].GetComponent<Renderer>().material.shader = Shader.Find("Standard");
                angleGameObjects[i].GetComponent<Renderer>().material.color = col;
                angleGameObjects[i].GetComponent<Renderer>().material.renderQueue = 3100;
            }
            List<Vector2> vertices2D = new List<Vector2>();
            vertices2D.Add(new Vector2(0, 0));
            for (float j = 0; System.Math.Round(j, 1) <= 1; j += 0.1f)
            {
                vertices2D.Add(Vector3.Slerp(forward, angleVec, j).normalized);
            }

            // Use the triangulator to get indices for creating triangles
            Triangulator tr = new Triangulator(vertices2D.ToArray());
            int[] indices = tr.Triangulate();

            // Create the Vector3 vertices
            Vector3[] vertices = new Vector3[vertices2D.Count];
            for (int j = 0; j < vertices.Length; j++)
            {
                vertices[j] = new Vector3(vertices2D[j].x, vertices2D[j].y, 0);
            }
            MeshFilter msh = angleGameObjects[i].GetComponent<MeshFilter>();
            msh.mesh.vertices = vertices;
            msh.mesh.triangles = indices;
            msh.mesh.RecalculateNormals();
            msh.mesh.RecalculateBounds();
            if (i==0)
                angleGameObjects[i].transform.position = jointsRefs[8].transform.position;
            else
                angleGameObjects[i].transform.position = jointsRefs[0].transform.position;
            //Debug.Log("Position: " + i.ToString() + angleGameObjects[i].transform.position.ToString());
        }
    }
	// Update is called once per frame
	void Update () {
        //UpdateAngles(new Vector3(1,0,0), new Vector3(0,0,1005));
    }
}
