﻿using System.Collections.Generic;

public class ThreadedVariables {
    public List<double[]> joints = new List<double[]>(); //Stores joint locations. Lock this variable to ensure there isn't a race condition
    public List<float[]> angles = new List<float[]>();  // Angles around joints e.g. arm flexion

    private static ThreadedVariables instance;

    public static ThreadedVariables Instance
    {
        get
        {
            if (instance == null)
            {
                // Instance singleton on first use.
                instance = new ThreadedVariables();
            }
            return instance;
        }
    }

}
