﻿using NDtw;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using vec = System.Numerics.Vector3;

///<summary>Fast DTW to determine when exercise was performed in real-time.
/// Reduced window size
/// Reduced set of points for GT and exmaple stream i.e. sampling
/// Runs on a worker thread started from main
/// </summary>
namespace DTW_Beta
{
    sealed class DTWQuick
    {
        static readonly DTWQuick dtwQuick = new DTWQuick();
        List<List<double>> dataE;
        List<List<double>> dataG;
        List<Tuple<string, Vector3D>> jointPositions;
        List<Tuple<int, int>> indicesOfJointPositions;
        public ExerciseCompletedEvent exerciseCompletedEvent;
        double minCost = 0.01; // TODO make this equal to the cost of the scaled GT
        double? prevNormCost;   // Previous normCost, if lower than normCost then previous DTW was end of exercise

        private DTWQuick()
        {
        }

        public static DTWQuick GetDtwQuick
        {
            get { return dtwQuick; }
        }

        public void InitDtwQuick(List<List<double>> _dataE, List<List<double>> _dataG, List<Tuple<string, Vector3D>> _jointPositions, List<Tuple<int, int>> _indicesOfJointPositions, double? _prevNormCost)
        {
            dataE = _dataE;
            dataG = _dataG;
            jointPositions = _jointPositions;
            indicesOfJointPositions = _indicesOfJointPositions;
            prevNormCost = _prevNormCost;
        }

        public void PerformFastDTW()
        {
            if (Monitor.TryEnter(dtwQuick))
            {
                try
                {
                    //double[] checkTroughs = new double[dataE[0].Count];
                    // What feature are we using to check for troughs
                    int feature = 0;
                    // What is the feature threshold to stop looking for troughs and set bExerciseInProgress to true
                    double featureThreshold = 0.45;
                    // Downsample signal used in DTW
                    List<List<double>> downsampledSignals = new List<List<double>>();
                    Tuple<List<int>, List<double>> res = SignalProcessing.DownsampleSignal(dataE[feature], 0.001);
                    for (int i = 0; i < dataE.Count; i++)
                    {
                        if (i == feature)
                        {
                            downsampledSignals.Add(res.Item2);
                            continue;
                        }
                        List<double> downsampledSignal = new List<double>();
                        for (int j = 0; j < res.Item1.Count; j++)
                            downsampledSignal.Add(dataE[i][res.Item1[j]]);
                        downsampledSignals.Add(downsampledSignal);

                    }

                    // TODO end of exercise code needs rewriting  
                    /*
                    if (!bCheckDTW)
                    {
                        // Check if exercise is in progress
                        if (!bExerciseInProgress && ((double[])_video[_video.Count - 1])[feature] > featureThreshold)
                            bExerciseInProgress = true;
                        else if (bExerciseInProgress && ((double[])_video[_video.Count - 1])[feature] <= featureThreshold)
                        {
                            bExerciseInProgress = false;
                            bCheckDTW = true;
                        }

                        // Check for troughs to segment stream
                        if (!bExerciseInProgress && !bCheckDTW)
                        {
                            // Store the variable stream in checkTroughs
                            for (int i = 0; i < _video.Count; i++)
                            {
                                checkTroughs[i] = ((double[])_video[i])[feature];
                            }
                            // Get the first two local minima from the stream. Used to segment the input into DTW
                            List<int> trimIndex = FindPeaks(checkTroughs, 10);
                            // If we have a local minima then trim the buffer
                            if (trimIndex != null)
                                _video = _video.GetRange(trimIndex[0], _video.Count - trimIndex[0]);
                        }
                    }*/
                    // simple smoothing code a(i + 1) = tiny * data(i + 1) + (1.0 - tiny) * a(i)
                    var seriesVariables = new List<SeriesVariable>();
                    // Add seriesVariables, divide by 3 for X, Y, Z
                    for (int i = 0; i < downsampledSignals.Count / 3; i++)
                    {
                        int j = i * 3;
                        // Get names from joints used in the direction vector and affix axis for each series variable
                        string dirNameX = jointPositions[indicesOfJointPositions[i].Item1].Item1 + jointPositions[indicesOfJointPositions[i].Item2].Item1 + " X";
                        string dirNameY = jointPositions[indicesOfJointPositions[i].Item1].Item1 + jointPositions[indicesOfJointPositions[i].Item2].Item1 + " Y";
                        string dirNameZ = jointPositions[indicesOfJointPositions[i].Item1].Item1 + jointPositions[indicesOfJointPositions[i].Item2].Item1 + " Z";
                        seriesVariables.Add(new SeriesVariable(downsampledSignals[j].ToArray(), dataG[j].ToArray(), dirNameX, new NDtw.Preprocessing.NonePreprocessor(), 1d));
                        seriesVariables.Add(new SeriesVariable(downsampledSignals[j + 1].ToArray(), dataG[j + 1].ToArray(), dirNameY, new NDtw.Preprocessing.NonePreprocessor(), 1d));
                        seriesVariables.Add(new SeriesVariable(downsampledSignals[j + 2].ToArray(), dataG[j + 2].ToArray(), dirNameZ, new NDtw.Preprocessing.NonePreprocessor(), 1d));
                    }

                    // TODO reduce the set of features used for DTW segmentation as not all features are necessary e.g. arm to side can use shoulderRightelbowRight X Y Z features only
                    var seriesVariablesArray = new List<SeriesVariable>() { seriesVariables[feature] }.ToArray(); // seriesVariables.ToArray(); new List<SeriesVariable>() { seriesVariables[feature] }.ToArray();

                    Dtw dtw = new Dtw(seriesVariablesArray, DistanceMeasure.Euclidean, false, true, 2, 20, 2);
                    // Run DTW to get the cost and normalise
                    double normCost = dtw.GetCost() / Math.Sqrt(dtw.XLength * dtw.XLength + dtw.YLength * dtw.YLength);
                    Tuple<int, int>[] dtwPath = null;
                    dtwPath = dtw.GetPath();
                    // Determine end of exercise when normalised cost drops below threshold
                    /*
                    if (normCost < 0.15)
                    {
                        normCosts.Add(new double[] { normCost, (double)dtwPath[0].Item1, (double)dtwPath[dtwPath.Length - 1].Item1 });
                        if (normCosts.Count >= trendSize)
                        {
                            int range = (normCosts.Count - trendSize);
                            double[] costsArray = normCosts.GetRange(range, trendSize).Select(x => x[0]).ToArray();
                            var seq = Enumerable.Range(0, trendSize).Select(x => (double)x).ToArray();
                            Tuple<double, double> res = MathNet.Numerics.Fit.Line(seq, costsArray);
                            // If slope is increasing confirm end of exercise
                            if (res.Item2 > 0)
                            {
                                numOfExercises++;
                                // TODO save all metrics and analyse exercise before clearing cost & clearing buffer
                                // Get mean angle of compensatory movement across exercise starting at dtwPath[0].Item1 to dtwPath[dtwPath.Count-1].Item1
                                // Find index of minimum cost
                                int minInd = 0;
                                for (int i = 0; i < normCosts.Count; i++)
                                {
                                    if (normCosts[i][0] < normCosts[minInd][0]) { minInd = i; }
                                }
                                // Get buffer
                                if (normCosts[normCosts.Count - 1][2] >= BufferSize)
                                {
                                    int offset = (int)(normCosts[minInd][2] + (normCosts.Count - minInd - 1)) - BufferSize;
                                    //costs[minInd][1] - offset;
                                    //costs[minInd][2] - offset;

                                }
                                // Update cost textbox
                                this.Dispatcher.Invoke((Action)(() =>
                                {
                                    costTB.Text = "Cost: " + normCosts[minInd][0].ToString();
                                }));
                                ExerciseCompletedEvent.Invoke();
                            }
                            // Update GUI with slope of line
                            this.Dispatcher.Invoke((Action)(() =>
                            {
                                lineTB.Text = "Slope: " + res.Item2.ToString();
                            }));
                        }
                    }*/
                    /*
                    if (bCheckDTW)
                    {
                        normCosts.Add(new double[] { normCost, (double)dtwPath[0].Item1, (double)dtwPath[dtwPath.Length - 1].Item1 });
                        // Fit line to last 15(half a second)/trendSize costs to see trend e.g. cost is decreasing/increasing etc
                        if (normCosts.Count >= trendSize)
                        {
                            int range = (normCosts.Count - trendSize);
                            double[] costsArray = normCosts.GetRange(range, trendSize).Select(x => x[0]).ToArray();
                            var seq = Enumerable.Range(0, trendSize).Select(x => (double)x).ToArray();
                            Tuple<double, double> res = MathNet.Numerics.Fit.Line(seq, costsArray);
                            // If slope is increasing confirm end of exercise
                            if (res.Item2 > 0 && false == true)
                            {
                                // TODO If DTW cost is low enough then class as a proper attempt
                                bCheckDTW = false;
                                numOfExercises++;
                                // TODO save all metrics and analyse exercise before clearing cost & clearing buffer
                                // Get mean angle of compensatory movement across exercise starting at dtwPath[0].Item1 to dtwPath[dtwPath.Count-1].Item1
                                // Find index of minimum cost
                                int minInd = 0;
                                for (int i = 0; i < normCosts.Count; i++)
                                {
                                    if (normCosts[i][0] < normCosts[minInd][0]) { minInd = i; }
                                }
                                // Get buffer
                                if (normCosts[normCosts.Count-1][2] >= BufferSize)
                                {
                                    int offset = (int)(normCosts[minInd][2] + (normCosts.Count - minInd - 1)) - BufferSize;
                                    //costs[minInd][1] - offset;
                                    //costs[minInd][2] - offset;

                                }
                                // Update cost textbox
                                this.Dispatcher.Invoke((Action)(() =>
                                {
                                    costTB.Text = "Cost: "+ normCosts[minInd][0].ToString();
                                }));
                                ExerciseCompletedEvent.Invoke();
                            }
                            // Update GUI with slope of line
                            this.Dispatcher.Invoke((Action)(() =>
                            {
                                lineTB.Text = "Slope: "+ res.Item2.ToString();
                            }));
                        }
                    }*/
                    //string s = normCost.ToString();
                    //results.Text = "Cost: " + s; //dtw.GetCost() / Math.Sqrt(xLength * xLength + yLength * yLength);

                    // Plot the signals and DTW
                    //if (bPlot)
                    //   plotWin.OnDtwChanged(dtw, dtwPath);
                    bool bExerciseCompleted = false;
                    double valueEndIndex = 0;
                    if (normCost < minCost && dtwPath[0].Item2 == 0 && prevNormCost != null && prevNormCost < normCost)
                    {
                        bExerciseCompleted = true;
                        valueEndIndex = downsampledSignals[feature][dtwPath[dtwPath.Length - 2].Item1];
                    }
                    exerciseCompletedEvent.Invoke(dtw, bExerciseCompleted, normCost, valueEndIndex, feature);
                    //Console.WriteLine("Cost: " + normCost);
                }
                finally
                {
                    // Ensure that the lock is released.
                    Monitor.Exit(dtwQuick);
                }
            }
        }
    }
}
