﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.Windows.Media.Media3D;

/// <summary>
/// Base class for compensatory movement
/// </summary>
/// <remarks>
/// Compensatory movements e.g. bent arm will inherit this class
/// </remarks>

namespace DTW_Beta
{
    public class CompMovmentBase
    {
        // Get angle between 2 2D vectors in radians
        public double GetAngleBetweenVectors(double v1x, double v1y, double v2x, double v2y)
        {
            return Math.Atan2(v2y - v1y, v2x - v1x);
        }

        // Calculate angle between 3 joints/2 bones
        public float CalculateAngle(Vector3D jointA, Vector3D connectingJoint, Vector3D jointB)
        {
            Vector3D boneA = Vector3D.Subtract(jointA, connectingJoint);
            Vector3D boneB = Vector3D.Subtract(jointB, connectingJoint);
            return (float)Vector3D.AngleBetween(boneA, boneB);
        }
    }
}
