﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using vec = System.Numerics.Vector3;

//<summary> Contians functions to improve accuracy of joint data </summary>

namespace DTW_Beta
{
    public static class JointPreprocessing
    {
        // Filter/Smooth a vector (pass by reference) with X,Y,Z representing previous vec values and alpha denoting amount to smooth
        public static void LowPassFilter(Vector3D vecToSmooth, float X, float Y, float Z, float alpha)
        {
            float invAlpha = 1.0f - alpha;
            vecToSmooth.X = alpha * vecToSmooth.X + invAlpha * X;
            vecToSmooth.Y = alpha * vecToSmooth.Y + invAlpha * Y;
            vecToSmooth.Z = alpha * vecToSmooth.Z + invAlpha * Z;
        }
    }
}
