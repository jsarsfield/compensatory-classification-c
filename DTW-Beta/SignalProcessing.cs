﻿using OxyPlot;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTW_Beta
{
    public static class SignalProcessing
    {
        // Downsample signal
        public static Tuple<List<int>,List<double>> DownsampleSignal(List<double> signal, double threshold) // Threshold of standard deviation in signal to determine if to add point to downsampled signal
        {
            List<double> downsampledSignal = new List<double>();
            List<int> indicies = new List<int>();
            List<double> variance = new List<double>();
            downsampledSignal.Add(signal[0]);
            indicies.Add(0);
            // Calculate rolling standard deviation over signal
            for (int i = 0; i < signal.Count - 1; i++)
            {
                variance.Add(signal[i + 1] - signal[i]);
                if (StandardDeviation(variance) > threshold)
                {
                    downsampledSignal.Add(signal[i]);
                    indicies.Add(i);
                    variance.Clear();
                }
            }
            downsampledSignal.Add(signal[signal.Count - 1]);
            indicies.Add(signal.Count - 1);
            return Tuple.Create(indicies,downsampledSignal);
        }
        // Produce a scaled ground truth to act as minimum cost for classifying exercise complete
        public static List<double> ScaleSignal(List<double> signal, double scale)
        {
            double offset = signal[0];
            List<double> scaledSignal = new List<double>();
            for (int i = 0; i < signal.Count; i++)
            {
                double res = signal[i] + ((offset - signal[i]) * scale);
                scaledSignal.Add(res);
            }
            return scaledSignal;
        }
        // Plot signal
        public static LineSeries PlotSignal(List<double> signal, OxyColor colour, OxyPlot.PlotModel model, LineSeries oldLineSeries = null)
        {
            // Remove old LineSeries
            if (oldLineSeries != null)
            {
                model.Series.Remove(oldLineSeries);
            }
            LineSeries plotSeries = new LineSeries { YAxisKey = "Direction", Color = colour };
            for (int i = 0; i < signal.Count; i++)
                plotSeries.Points.Add(new DataPoint(i, signal[i]));
            model.Series.Add(plotSeries);
            model.InvalidatePlot(true);
            return plotSeries;
        }
        // Get standard deviation of List
        public static double StandardDeviation(IEnumerable<double> values)
        {
            double avg = values.Average();
            return Math.Sqrt(values.Average(v => Math.Pow(v - avg, 2)));
        }
    }
}
