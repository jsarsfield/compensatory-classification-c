﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections;
using System.ComponentModel;
using NDtw;

namespace DTW_Beta
{
    /// <summary>
    /// Interaction logic for SliceGroundTruthWindow.xaml
    /// </summary>
    public partial class SliceGroundTruthWindow : Window
    {
        public PlotModel Model { get; set; }
        string YAxisKey = "Direction";
        List<List<double>> seq;
        List<List<double>> modifiedSeq = new List<List<double>>();
        List<List<double>> outputSeq = new List<List<double>>();
        string GestureSaveFileLocation;
        LineSeries l1;
        LineSeries l2;
        LineSeries downsampledLS;   // Stores lineseries, used to update lineseries
        LineSeries scaledLS;
        LineSeries dtwLS;
        int startIndex = 0;
        int endIndex;
        // Which feature to display (axis of direction vector)
        int feature = 0;
        BindingList<Tuple<string, string>> tempDirectionVecsUsedInDTW;

        public SliceGroundTruthWindow(String SaveLoc)
        {
            InitializeComponent();
            Model = new PlotModel { Title = "Ground Truth Visualisation" };
            Model.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Key = YAxisKey });
            Model.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, Key = "Index" });
            this.DataContext = this;
            GestureSaveFileLocation = SaveLoc;
            // Create OpenFileDialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            // Set filter for file extension and default file extension
            dlg.DefaultExt = ".bin";
            dlg.Filter = "Binary files (.bin)|*.bin";
            dlg.InitialDirectory = SaveLoc;
            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();
            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                // 
                IFormatter formatter = new BinaryFormatter();
                Stream stream = new FileStream(dlg.FileName,
                                          FileMode.Open,
                                          FileAccess.Read,
                                          FileShare.Read);
                Hashtable dataLoaded = (Hashtable)formatter.Deserialize(stream);
                tempDirectionVecsUsedInDTW = (BindingList<Tuple<string, string>>)dataLoaded[1];
                seq = (List<List<double>>)dataLoaded[2];
                stream.Close();
            }
            endIndex = seq.Count - 1;
            // Plot GT
            SignalProcessing.PlotSignal(seq[feature], OxyColors.Black, Model);
            OutputCopy(seq);
            // If mouse down then get index of series for slicing
            Model.Series[0].MouseDown += (s, e) =>
            {
                if (e.ChangedButton == OxyMouseButton.Left)
                {
                    // If not control down then index is set to startIndex
                    if (!e.IsControlDown)
                    {
                        int indexOfNearestPoint = (int)Math.Round(e.HitTestResult.Index);
                        startIndex = indexOfNearestPoint;
                        if (l1 != null)
                            Model.Series.Remove(l1);
                        l1 = new LineSeries { YAxisKey = YAxisKey, ItemsSource = new List<DataPoint> { new DataPoint(indexOfNearestPoint, -1), new DataPoint(indexOfNearestPoint, 1) }, Color = OxyPlot.OxyColors.Black };
                        Model.Series.Add(l1);
                        Model.InvalidatePlot(true);
                    }
                    else
                    {
                        int indexOfNearestPoint = (int)Math.Round(e.HitTestResult.Index);
                        endIndex = indexOfNearestPoint+1;

                        if (l2 != null)
                            Model.Series.Remove(l2);
                        l2 = new LineSeries { YAxisKey = YAxisKey, ItemsSource = new List<DataPoint> { new DataPoint(indexOfNearestPoint, -1), new DataPoint(indexOfNearestPoint, 1) }, Color = OxyPlot.OxyColors.Red };
                        Model.Series.Add(l2);
                        Model.InvalidatePlot(true);
                    }
                    OutputCopy(seq,startIndex,endIndex);
                }
            };
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            /*
            List<List<double>> tempSeq = new List<List<double>>(seq.Count);
            for (int j = 0; j < seq.Count; j++)
            {
                tempSeq.Add(new List<double>());
                for (int i = startIndex; i <= endIndex; i++)
                {
                    tempSeq[j].Add(seq[j][i]);
                }
            }*/
            if (filenameTB.Text != "")
            {
                IFormatter formatter = new BinaryFormatter();
                Stream stream = new FileStream(GestureSaveFileLocation + filenameTB.Text + ".bin",
                                         FileMode.Create,
                                         FileAccess.Write, FileShare.None);
                Hashtable dataToSave = new Hashtable();
                dataToSave.Add(1, tempDirectionVecsUsedInDTW);
                dataToSave.Add(2, outputSeq);
                formatter.Serialize(stream, dataToSave);
                stream.Close();
                filenameTB.Text = "";
            }
        }

        private void btnDownsample_Click(object sender, RoutedEventArgs e)
        {
            modifiedSeq.Clear();
            double threshold;
            // If can cast to double
            if (double.TryParse(downsampleTB.Text, out threshold))
            {
                Tuple<List<int>, List<double>> res = SignalProcessing.DownsampleSignal(seq[feature], threshold);
                for (int i = 0; i < seq.Count; i++)
                {
                    if (i == feature)
                    {
                        modifiedSeq.Add(res.Item2);
                        continue;
                    }
                    List<double> downsampledSignal = new List<double>();
                    for (int j = 0; j < res.Item1.Count; j++)
                        downsampledSignal.Add(seq[i][res.Item1[j]]);
                    modifiedSeq.Add(downsampledSignal);
                }
                downsampledLS = SignalProcessing.PlotSignal(modifiedSeq[feature], OxyColors.Red, Model, downsampledLS);
                OutputCopy(modifiedSeq);
            }
        }

        private void btnScale_Click(object sender, RoutedEventArgs e)
        {
            double scale;
            string[] dtwParams = dtwparamsTB.Text.Replace("  ", string.Empty).Split(',');
            // If can cast to double
            if (double.TryParse(scaleTB.Text, out scale))
            {
                scale = 1 - scale;
                var scaled = SignalProcessing.ScaleSignal(outputSeq[feature], scale);
                scaledLS = SignalProcessing.PlotSignal(scaled, OxyColors.Red, Model, scaledLS);
                var seriesVariables = new List<SeriesVariable>();
                seriesVariables.Add(new SeriesVariable(scaled.ToArray(), outputSeq[feature].ToArray(), "scaledCost", new NDtw.Preprocessing.NonePreprocessor(), 1d));
                Dtw dtw = new Dtw(seriesVariables.ToArray(), DistanceMeasure.Euclidean, Convert.ToBoolean(dtwParams[0]), Convert.ToBoolean(dtwParams[1]), Int32.Parse(dtwParams[2]), Int32.Parse(dtwParams[3]), Int32.Parse(dtwParams[4]));
                costT.Text = " Cost: " + dtw.GetCost() + " Norm Cost: " + (dtw.GetCost() / Math.Sqrt(dtw.XLength * dtw.XLength + dtw.YLength * dtw.YLength)).ToString();
                if (dtwLS != null)
                    Model.Series.Remove(dtwLS);
                dtwLS = new LineSeries
                {
                    YAxisKey = YAxisKey,
                    StrokeThickness = 0.5,
                    Color = OxyColors.DimGray,
                };
                for (int i = 0; i < dtw.GetPath().Length; i++)
                {
                    dtwLS.Points.Add(new DataPoint(dtw.GetPath()[i].Item1, dtw.SeriesVariables[0].GetPreprocessedXSeries()[dtw.GetPath()[i].Item1]));
                    dtwLS.Points.Add(new DataPoint(dtw.GetPath()[i].Item2, dtw.SeriesVariables[0].GetPreprocessedYSeries()[dtw.GetPath()[i].Item2]));
                    dtwLS.Points.Add(new DataPoint(double.NaN, double.NaN));
                }
                Model.Series.Add(dtwLS);
                Model.InvalidatePlot(true);
            }
        }

        // Deep copy array to outputSeq
        private void OutputCopy(List<List<double>> arrToCopy, int start = 0, int end = -1)
        {
            // If no start or end index params passed then use length of first variable in arrToCopy
            if (end == -1)
                end = arrToCopy[0].Count;
            outputSeq.Clear();
            for (int i = 0; i < arrToCopy.Count; i++)
            {
                outputSeq.Add(new List<double>());
                for (int j = start; j < end; j++)
                {
                    outputSeq[outputSeq.Count - 1].Add(arrToCopy[i][j]);
                }
            }
        }
    }
}
