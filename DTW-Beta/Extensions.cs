﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace DTW_Beta
{
    public static class Extensions
    {
        public static Vector3D GetJointPosition(List<Tuple<string,Vector3D>> joints, string joint)
        {
            for(int i = 0; i < joints.Count; i++)
            {
                if (joints[i].Item1 == joint)
                    return joints[i].Item2;
            }
            return new Vector3D();
        }
    }
}
