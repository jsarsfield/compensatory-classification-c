﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DTW_Beta
{
    /* (Singleton class) Anonymous pipe for sending data to unity CASR process
    Transport method is thread safe e.g. locked */
    sealed class IPC
    {
        static readonly IPC ipc = new IPC();
        private Process pipeClient = new Process();
        private AnonymousPipeServerStream pipeServer;
        private BinaryFormatter formatter = new BinaryFormatter(); // Serailise data and write to stream for passing to Unity process

        //Constructor - Start Unity CASR process and setup anon pipe
        private IPC()
        {
            pipeClient.StartInfo.FileName = "casr.exe";
            pipeServer = new AnonymousPipeServerStream(PipeDirection.Out, HandleInheritability.Inheritable);

            // Pass the client process a handle to the server.
            pipeClient.StartInfo.Arguments = pipeServer.GetClientHandleAsString();
            pipeClient.StartInfo.UseShellExecute = false;
            pipeClient.Start();

            pipeServer.DisposeLocalCopyOfClientHandle(); // If this method is not called, the AnonymousPipeServerStream object will not receive notice when the client disposes of its PipeStream object.
        }

        public static void SendToUnity(Dictionary<string,object> obj)
        {
            Task.Run(() => ipc.SendMessage(obj));
        }

        public static IPC GetIPC
        {
            get { return ipc; }
        }

        //Anon pipe test
        public void SendMessage(Dictionary<string, object> obj)
        {
            // Try and Request lock, give up if takes longer than 15 milliseconds (Kinect creates new frame every 33ms)
            if (Monitor.TryEnter(this,15))
            {
                // Try to send data to client (Unity process)
                try
                {
                    formatter.Serialize(pipeServer, obj);
                }
                // Catch the IOException that is raised if the pipe is broken or disconnected.
                catch (IOException e)
                {
                    Console.WriteLine("[SERVER] Error: {0}", e.Message);
                }
                finally
                {
                    // Ensure that the lock is released.
                    Monitor.Exit(this);
                }
                return;
            }
            else
            {
                return;
            }  
        }

        ~IPC()
        {
            if (!pipeClient.HasExited)
                pipeClient.Kill();
            pipeClient.Close();
        }
    }
}
