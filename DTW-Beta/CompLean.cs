﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace DTW_Beta
{
    class CompLean : CompMovmentBase
    {
        public double GetMeanAngleOfLean()
        {
            // angle in radians between 2 3d vectors is subspace in Matlab same as acos(abs(A'*B))
            return 0;
        }
        public double GetAngleOfLean(Vector2 spineBase, Vector2 spineMid)
        {
            return GetAngleBetweenVectors(spineBase.X, spineBase.Y, spineMid.X, spineMid.Y);
        }
    }
}
