﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace DTW_Beta
{
    class Skeleton2DDataCoordEventArgs
    {
        // Positions of the elbows, the wrists and the hands (placed from left to right)
        private readonly Point[] _points;

        // Initializes a new instance of the Skeleton2DD ataCoordEventArgs class
        public Skeleton2DDataCoordEventArgs(Point[] points)
        {
            _points = (Point[])points.Clone();
        }

        // Gets the point at a certain index
        public Point GetPoint(int index)
        {
            return _points[index];
        }

        // Gets the coordinates of our _points
        internal double[] GetCoords()
        {
            var tmp = new double[_points.Length * 2];
            for (int i = 0; i < _points.Length; i++)
            {
                tmp[2 * i] = _points[i].X;
                tmp[(2 * i) + 1] = _points[i].Y;
            }

            return tmp;
        }
    }
}
