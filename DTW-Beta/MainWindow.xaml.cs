﻿#define LOAD_DEFAULT_GESTURES
#define bCapture3d

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections;
using Microsoft.Kinect;
using System.IO;
using Microsoft.Speech.Recognition;
using Microsoft.Speech.AudioFormat;
using System.Xml;
using NDtw;
using System.Linq;
using System.Threading;
using System.Windows.Threading;

namespace DTW_Beta
{
    using System.ComponentModel;
    using System.Diagnostics;
    using System.IO.Pipes;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Threading.Tasks;
    using System.Windows.Media.Media3D;

#if bCapture3d
    using vec = System.Numerics.Vector3;
#else
    using vec = System.Numerics.Vector2;
#endif
    public delegate void ExerciseCompletedEvent(Dtw _dtw, bool bExerciseCompleted, double? prevNormCost, double valueEndIndex, int feature);   // Delegate for exercsie completed event

    public partial class MainWindow : Window
    {
        private const int BufferSize = 256;
        private const int MinimumFrames = 6;
        private const int CaptureCountdownSeconds = 3;
        private string GestureSaveFileLocation = System.IO.Path.GetFullPath(Environment.CurrentDirectory+ "\\..\\");
        private const string GestureSaveFileNamePrefix = @"RecordedGestures";
        private bool _capturing;
        public List<List<double>> dataE; // Store stream from kinect. Used for DTW and is compared against dataG
        public List<List<double>> dataG;    // Array containing ground truth data
        private DateTime _captureCountdown = DateTime.Now;
        private System.Windows.Forms.Timer _captureCountdownTimer;
        private KinectSensor _Kinect;
        bool bSpeech = false;
        bool bPlot = true;
        //List<double[]> normCosts = new List<double[]>();
        // Is the exercise in progress
        bool bExerciseInProgress = false;
        // If near the end of exercise then start checking lowest cost using DTW
        bool bCheckDTW = false;
        // When exercise is complete
        //public event EventHandler ExerciseCompletedEvent;
        // Exercise count
        int numOfExercises = 0;
        // Size of the costs array to check the trend of the line
        int trendSize = 15;
        Dtw dtw;
        int reduceFrames = 0;   // Remove every other frame e.g. reduceFrames%2 == 0 then keep frame
        double? prevNormCost = null;

        public List<string> jointsList = new List<string> { "ShoulderRight", "ElbowRight", "WristRight", "SpineShoulder", "ShoulderLeft", "ElbowLeft", "WristLeft", "SpineBase", "SpineMid", "Neck", "Head", "HipLeft", "HipRight" };
        public BindingList<Tuple<string, string>> directionVecsUsedInDTW = new BindingList<Tuple<string, string>>();  // Feature direction vectors used in DTW. This variable is saved along with GT data
        public BindingList<CompMovmentBase> compensatoryMovements = new BindingList<CompMovmentBase>(); // List of compensatory movements associated with the exercise
        public List<Tuple<int, int>> indicesOfJointPositions = new List<Tuple<int, int>>(); // indicesOfJointPositions is populated on the first frame from Kinect as we will then know the order the joints are processed
        int numberOfUniqueJoints; // Number of unique joints used in DTW
        private ColorFrameReader _readerColour;
        private DepthFrameReader _readerDepth;
        private BodyFrameReader _readerBody;
        private BodyIndexFrameReader _readerBIFS;
        CameraMode _mode = CameraMode.Color;
        IList<Body> _bodies;
        private WriteableBitmap _ColorImageBitmap;
        private Int32Rect _ColorImageBitmapRect;
        private int _ColorImageStride;

        // The last recognized gesture time
        private DateTime _lastGestureTime = DateTime.MinValue;

        private KinectAudioStream convertStream = null;
        private SpeechRecognitionEngine speechEngine = null;

        // Minimum time difference between two gestures.
        private TimeSpan _minimumTimeDifference = TimeSpan.FromSeconds(1);
        public PlotWindow plotWin;
        public Thread plotWinThread;
        
        //event ExerciseCompletedEvent exerciseCompletedEvent;  // Event raised when DTWQuick detects exercise

        public MainWindow()
        {
            //Console.WriteLine(Process.GetProcessesByName("vshost32").First().ToString());
            InitializeComponent();
            this.cbStartJoint.DataContext = jointsList; // Bind jointsList to start combobox
            this.cbEndJoint.DataContext = jointsList; // Bind jointsList to end combobox
            this.lbVarsToRecord.DataContext = directionVecsUsedInDTW;  // Bind directionVecsUsedInDTW to listbox
            this.lbCompMovements.DataContext = compensatoryMovements;  // Bind compensatoryMovements to listbox
            directionVecsUsedInDTW.ListChanged += directionVecsUsedInDTW_ListChanged;
            //this.exerciseCompletedEvent += new ExerciseCompletedEvent(OnExerciseCompletedEvent);   // Register event with callback method
            // Create a thread for plot window
            plotWinThread = new Thread(new ThreadStart(() =>
            {
                // Create our context, and install it:
                SynchronizationContext.SetSynchronizationContext(
                    new DispatcherSynchronizationContext(
                        Dispatcher.CurrentDispatcher));

                plotWin = new PlotWindow();
                // When the window closes, shut down the dispatcher
                plotWin.Closed += (s, e) =>
                   Dispatcher.CurrentDispatcher.BeginInvokeShutdown(DispatcherPriority.Background);

                plotWin.Show();
                // Start the Dispatcher Processing
                System.Windows.Threading.Dispatcher.Run();
            }));
            // Set the apartment state
            plotWinThread.SetApartmentState(ApartmentState.STA);
            // Make the thread a background thread
            plotWinThread.IsBackground = true;
            // Start the thread
            plotWinThread.Start();

           // Console.WriteLine(OpencvWrapper.add(1, 10).ToString());
        }
        // When directionVecsUsedInDTW changes clear indicesOfJointPositions
        void directionVecsUsedInDTW_ListChanged(object sender, ListChangedEventArgs e)
        {
            // Create new dataE array
            dataE = new List<List<double>>(directionVecsUsedInDTW.Count * 3);
            for (int i = 0; i < directionVecsUsedInDTW.Count * 3; i++) { dataE.Add(new List<double>()); }

            indicesOfJointPositions.Clear();
            // Determine number of unique joints
            List<string> temp = new List<string>();
            foreach (Tuple<string, string> v in directionVecsUsedInDTW)
            {
                temp.Add(v.Item1);
                temp.Add(v.Item2);
            }
            numberOfUniqueJoints = temp.Distinct().Count();
        }
        // Clear video frames buffer when mouse back button is clicked
        private void MouseEvent(object sender, MouseButtonEventArgs e)
        {
            switch (e.ChangedButton)
            {
                case MouseButton.XButton1:
                    break;
                default:
                    break;
            }
        }
        // Initialise dtw object
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Kinect = KinectSensor.GetDefault();
            //ExerciseCompletedEvent += new EventHandler(ExerciseCompleted);
        }

        // Record exercise ground truth
        private void btnCapture_Click(object sender, RoutedEventArgs e)
        {
            _captureCountdown = DateTime.Now.AddSeconds(CaptureCountdownSeconds);
            _captureCountdownTimer = new System.Windows.Forms.Timer();
            _captureCountdownTimer.Interval = 50;
            _captureCountdownTimer.Start();
            _captureCountdownTimer.Tick += CaptureCountdown;
        }
        // Stop recording exercise ground truth
        private void btnStore_Click(object sender, RoutedEventArgs e)
        {
            btnCapture.IsEnabled = true;
            btnStore.IsEnabled = false;
            // Deep copy dataE to dataG
            dataG = new List<List<double>>();
            for (int i = 0; i < dataE.Count; i++)
                dataG.Add(dataE[i].ToList());
            results.Text = "Gesture added.";
            // Clear the dataE buffer
            ClearDataEBuffer(null);

            btnCapture.IsEnabled = true;
            btnStore.IsEnabled = false;
            // Set the capturing? flag
            _capturing = false;
            // Update the status display
            lblStatus.Text = "Reading";
        }
        // Load exercise ground truth
        private void btnLoadFile_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            // Set filter for file extension and default file extension
            dlg.DefaultExt = ".bin";
            dlg.Filter = "Binary files (.bin)|*.bin";
            dlg.InitialDirectory = GestureSaveFileLocation;
            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();
            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                // 
                IFormatter formatter = new BinaryFormatter();
                Stream stream = new FileStream(dlg.FileName,
                                          FileMode.Open,
                                          FileAccess.Read,
                                          FileShare.Read);
                Hashtable dataLoaded = (Hashtable)formatter.Deserialize(stream);
                RepopulateDirectionVecsUsedInDTW((BindingList<Tuple<string, string>>)dataLoaded[1]);
                dataG = (List<List<double>>)dataLoaded[2];
                stream.Close();    
                lblStatus.Text = "Gestures loaded!";
            }
        }
        // Save exercise ground truth
        private void btnSaveToFile_Click(object sender, RoutedEventArgs e)
        {
            string fileName = GestureSaveFileNamePrefix + DateTime.Now.ToString("yyyy-MM-dd_HH-mm") + ".bin";
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(GestureSaveFileLocation + fileName,
                                     FileMode.Create,
                                     FileAccess.Write, FileShare.None);
            Hashtable dataToSave = new Hashtable();
            dataToSave.Add(1, directionVecsUsedInDTW);
            dataToSave.Add(2, dataG);
            formatter.Serialize(stream, dataToSave);
            stream.Close();
            lblStatus.Text = "Saved to " + fileName;
        }
        // Repopulate using this method to ensure we don't lose the binding
        private void RepopulateDirectionVecsUsedInDTW(BindingList<Tuple<string, string>> temp)
        {
            directionVecsUsedInDTW.Clear();
            foreach (Tuple<string, string> i in temp)
            {
                directionVecsUsedInDTW.Add(i);
            }
        }
        // Open slice ground truth window
        private void btnSliceGT_Click(object sender, RoutedEventArgs e)
        {
            new SliceGroundTruthWindow(GestureSaveFileLocation).Show();
        }    
        // End process including worker threads when window closes
        private void WindowClosing(object sender, EventArgs e)
        {

            Environment.Exit(Environment.ExitCode);
        }
        // Add variable to the list of variables that are being recorded for the ground truth e.g. shoulderRight to elbowRight
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            // If selected start joint and end joint are not empty then add to listbox
            if (this.cbStartJoint.SelectedIndex != -1 && this.cbEndJoint.SelectedIndex != -1)
            {
                // If we already have the variables then return
                if (directionVecsUsedInDTW.Any(v => v.Item1 == this.cbStartJoint.SelectedValue.ToString() && v.Item2 == this.cbEndJoint.SelectedValue.ToString()))
                    return;
                directionVecsUsedInDTW.Add(new Tuple<string, string>(this.cbStartJoint.SelectedValue.ToString(), this.cbEndJoint.SelectedValue.ToString()));
            }
        }
        // Remove variable from the list of variables that are being recorded for the ground truth
        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            if (this.lbVarsToRecord.SelectedIndex != -1)
                directionVecsUsedInDTW.RemoveAt(this.lbVarsToRecord.SelectedIndex);
        }  
        // Open kinect sensor streams
        private void InitializeKinectSensor(KinectSensor sensor)
        {
            if (sensor != null)
            {
                _readerColour = _Kinect.ColorFrameSource.OpenReader();
                _readerColour.FrameArrived += Reader_ColourFrameArrived;

                _readerDepth = _Kinect.DepthFrameSource.OpenReader();
                _readerDepth.FrameArrived += Reader_DepthFrameArrived;

                _readerBody = _Kinect.BodyFrameSource.OpenReader();
                _readerBody.FrameArrived += Reader_BodyFrameArrived;

                _readerBIFS = _Kinect.BodyIndexFrameSource.OpenReader();
                _readerBIFS.FrameArrived += Reader_BIFSArrived;

                this._ColorImageBitmap = new WriteableBitmap(sensor.ColorFrameSource.FrameDescription.Width,
                                                sensor.ColorFrameSource.FrameDescription.Height, 96, 96,
                                                PixelFormats.Bgr32, null);
                this._ColorImageBitmapRect = new Int32Rect(0, 0, sensor.ColorFrameSource.FrameDescription.Width,
                sensor.ColorFrameSource.FrameDescription.Height);
                this._ColorImageStride = sensor.ColorFrameSource.FrameDescription.Width * (int)sensor.ColorFrameSource.FrameDescription.BytesPerPixel; 

                sensor.Open();

                // Initialise Audio
                // grab the audio stream
                IReadOnlyList<AudioBeam> audioBeamList = _Kinect.AudioSource.AudioBeams;
                System.IO.Stream audioStream = audioBeamList[0].OpenInputStream();

                // create the convert stream
                this.convertStream = new KinectAudioStream(audioStream);
                RecognizerInfo ri = TryGetKinectRecognizer();
                if (null != ri)
                {
                    this.speechEngine = new SpeechRecognitionEngine(ri.Id);
                    // Create a grammar from grammar definition XML file.
                    XmlDocument doc = new XmlDocument();
                    doc.Load("SpeechGrammar.xml");
                    using (var memoryStream = new MemoryStream(Encoding.ASCII.GetBytes(doc.OuterXml)))
                    {
                        var g = new Grammar(memoryStream);
                        this.speechEngine.LoadGrammar(g);
                    }

                    this.speechEngine.SpeechRecognized += this.SpeechRecognized;

                    // let the convertStream know speech is going active
                    this.convertStream.SpeechActive = true;

                    this.speechEngine.SetInputToAudioStream(
                    this.convertStream, new SpeechAudioFormatInfo(EncodingFormat.Pcm, 16000, 16, 1, 32000, 2, null));
                    this.speechEngine.RecognizeAsync(RecognizeMode.Multiple);
                }
            }
        }
        
        public KinectSensor Kinect
        {
            get { return this._Kinect; }
            set
            {
                this._Kinect = value;
                InitializeKinectSensor(this._Kinect);
            }
        }

        private void Reader_ColourFrameArrived(object sender, ColorFrameArrivedEventArgs e)
        {
            ColorFrameReference colRef = e.FrameReference;

            // Color
            using (ColorFrame frame = colRef.AcquireFrame())
            {
                if (frame != null)
                {
                    /*
                    int size = frame.FrameDescription.Width * frame.FrameDescription.Height * KinectCoordinateMapping.Constants.BYTES_PER_PIXEL;
                    IntPtr pnt = Marshal.AllocHGlobal(size);
                    byte[] imageData = new byte[size];
                    frame.CopyConvertedFrameDataToArray(imageData, ColorImageFormat.Bgra);
                    Marshal.Copy(imageData, 0, pnt, size);
                    OpencvWrapper.DrawImage(pnt, size, frame.FrameDescription.Width, frame.FrameDescription.Height);
                    Marshal.FreeHGlobal(pnt);*/

                    byte[] _pixels = new byte[frame.FrameDescription.Width * frame.FrameDescription.Height * KinectCoordinateMapping.Constants.BYTES_PER_PIXEL];
                    frame.CopyConvertedFrameDataToArray(_pixels, ColorImageFormat.Rgba);
                    IPC.SendToUnity(new Dictionary<string, object> {{"video", _pixels}});
                    frame.Dispose();
                    //camera.Source = KinectCoordinateMapping.ColorExtensions.ToBitmap(frame);
                }
            }
        }

        private void Reader_DepthFrameArrived(object sender, DepthFrameArrivedEventArgs e)
        {
            DepthFrameReference depthRef = e.FrameReference;

            // Depth
            using (DepthFrame frame = depthRef.AcquireFrame())
            {
                if (frame != null)
                {
                    //camera.Source = KinectCoordinateMapping.DepthExtensions.ToBitmap(frame);
                }
            }
        }

        private void Reader_BIFSArrived(object sender, BodyIndexFrameArrivedEventArgs e)
        {
            BodyIndexFrameReference bifRef = e.FrameReference;

            // Body index frame
            using (BodyIndexFrame frame = bifRef.AcquireFrame())
            {
                if (frame != null)
                {
                    //camera.Source = KinectCoordinateMapping.DepthExtensions.ToBitmap(frame);
                }
            }
        }
        private Polyline GetBodySegment(IReadOnlyDictionary<JointType, Joint> joints, Brush brush, JointType[] jt)
        {
            PointCollection points = new PointCollection(jt.Length);
            foreach (var jointType in jt)
            {
                points.Add(GetDisplayPosition(joints[jointType]));
            }
            Polyline polyline = new Polyline();
            polyline.Points = points;
            polyline.Stroke = brush;
            polyline.StrokeThickness = 2;
            return polyline;
        }

        private Point GetDisplayPosition(Joint joint)
        {
            ColorSpacePoint csp = _Kinect.CoordinateMapper.MapCameraPointToColorSpace(joint.Position);
            return new Point(csp.X, csp.Y);
        }

        // Callback function when joint data arrives from Kinect
        private void Reader_BodyFrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {
            var jointPositions = new List<Tuple<string, Vector3D>>();
            BodyFrameReference bodyRef = e.FrameReference;
            // Loop bodies, draw joints of first body and calculate input feaures/driection vectors
            using (BodyFrame frame = bodyRef.AcquireFrame())
            {
                if (frame != null)
                {
                    canvas.Children.Clear();

                    _bodies = new Body[frame.BodyFrameSource.BodyCount];

                    frame.GetAndRefreshBodyData(_bodies);
                    // Get body that first joint is closest to centre of screen along X axis
                    Body body = null;
                    float? nearestToZero = null;
                    int? indexOfnearest = null;
                    for (int i = 0; i < _bodies.Count; i++)
                    {
                        if (_bodies[i].IsTracked)
                        {
                            if (nearestToZero == null)
                            {
                                indexOfnearest = i;
                                nearestToZero = Math.Abs(_bodies[i].Joints.Values.First().Position.X);
                            }
                            else
                            {
                                float val = Math.Abs(_bodies[i].Joints.Values.First().Position.X);
                                if (val < nearestToZero)
                                {
                                    indexOfnearest = i;
                                    nearestToZero = val;
                                }
                            }
                        }
                    }
                    
                    if (indexOfnearest != null)
                    {
                        body = _bodies[(int)indexOfnearest];
                        List<double[]> jointPoints = new List<double[]>();
                        // COORDINATE MAPPING
                        foreach (Joint joint in body.Joints.Values)
                        {
                            if (joint.TrackingState == TrackingState.Tracked || joint.TrackingState == TrackingState.Inferred)
                            {
                                if (directionVecsUsedInDTW.Any(v => v.Item1 == joint.JointType.ToString() || v.Item2 == joint.JointType.ToString()))
                                {
                                    #if  bCapture3d
                                        jointPositions.Add(Tuple.Create(joint.JointType.ToString(),new Vector3D(joint.Position.X, joint.Position.Y, joint.Position.Z)));
                                    #else
                                        jointPositions.Add(Tuple.Create(joint.JointType.ToString(),new vec(joint.Position.X, joint.Position.Y)));
                                    #endif
                                }
                                // 3D space point
                                CameraSpacePoint jointPosition = joint.Position;

                                // 2D space point
                                Point point = new Point();

                                if (_mode == CameraMode.Color)
                                {
                                    ColorSpacePoint colorPoint = _Kinect.CoordinateMapper.MapCameraPointToColorSpace(jointPosition);

                                    point.X = float.IsInfinity(colorPoint.X) ? 0 : colorPoint.X;
                                    point.Y = float.IsInfinity(colorPoint.Y) ? 0 : colorPoint.Y;
                                }
                                else if (_mode == CameraMode.Depth || _mode == CameraMode.Infrared) // Change the Image and Canvas dimensions to 512x424
                                {
                                    DepthSpacePoint depthPoint = _Kinect.CoordinateMapper.MapCameraPointToDepthSpace(jointPosition);

                                    point.X = float.IsInfinity(depthPoint.X) ? 0 : depthPoint.X;
                                    point.Y = float.IsInfinity(depthPoint.Y) ? 0 : depthPoint.Y;
                                }

                                Ellipse ellipse;
                                // Draw
                                ellipse = new Ellipse
                                {
                                    Fill = Brushes.Red,
                                    Width = 20,
                                    Height = 20
                                };

                                // If the joint is in the list of joints we are displaying in unity then add to list
                                if (jointsList.Contains(joint.JointType.ToString()))
                                {
                                    jointPoints.Add(new double[] { point.X, point.Y });
                                }
                                        
                                Canvas.SetLeft(ellipse, point.X - ellipse.Width / 2);
                                Canvas.SetTop(ellipse, point.Y - ellipse.Height / 2);

                                canvas.Children.Add(ellipse);
                            }
                        }
                        // Send joint data to unity
                        IPC.SendToUnity(new Dictionary<string, object>{{"joints", jointPoints}});
                        if (directionVecsUsedInDTW.Count > 0)
                            IPC.SendToUnity(new Dictionary<string, object>{{"angles", new List<float>() { new CompArmFlexion().CalculateAngle(Extensions.GetJointPosition(jointPositions, "ShoulderRight"), Extensions.GetJointPosition(jointPositions, "ElbowRight"), Extensions.GetJointPosition(jointPositions, "WristRight")),
                            new CompLean().CalculateAngle(Extensions.GetJointPosition(jointPositions, "SpineMid"), Extensions.GetJointPosition(jointPositions, "SpineBase"), Extensions.GetJointPosition(jointPositions, "HipRight"))} }});

                        // Skip x amount of frames for DTW
                        reduceFrames++;
                        //if (reduceFrames % 2 == 0)
                        //    return;
                        #if bCapture3d
                            double[] jointFeatures = new double[directionVecsUsedInDTW.Count * 3];
                        #else
                            double[] jointFeatures = new double[directionVecsUsedInDTW.Count * 2];
                        #endif
                        // if we have positions for all directionVecsUsedInDTW then continue
                        if (jointPositions.Count == numberOfUniqueJoints)
                        {
                            // If indicesOfJointPositions is empty then find indicies of joints in directionVecsUsedInDTW (Only needs to be done on first frame from Kinect)
                            if (indicesOfJointPositions.Count == 0)
                            {
                                for (int i = 0; i < directionVecsUsedInDTW.Count; i++)
                                {
                                    int firstIndex = -1;
                                    int secondIndex = -1;
                                    for (int j = 0; j < jointPositions.Count; j++)
                                    {
                                        if (jointPositions[j].Item1 == directionVecsUsedInDTW[i].Item1)
                                        {
                                            firstIndex = j;
                                        } else if (jointPositions[j].Item1 == directionVecsUsedInDTW[i].Item2)
                                        {
                                            secondIndex = j;
                                        }
                                        if (firstIndex != -1 && secondIndex != -1)
                                            break;
                                    }
                                    indicesOfJointPositions.Add(Tuple.Create(firstIndex, secondIndex));
                                }
                            }
                            // Get normalised direction vectors between joints
                            for (int i = 0; i < indicesOfJointPositions.Count; i++)
                            {
#if bCapture3d
                                Vector3D t = Vector3D.Subtract(jointPositions[indicesOfJointPositions[i].Item2].Item2, jointPositions[indicesOfJointPositions[i].Item1].Item2);
                                t.Normalize();
                                //Vector3D t = GetNormalisedDirection3d(jointPositions[indicesOfJointPositions[i].Item1].Item2, jointPositions[indicesOfJointPositions[i].Item2].Item2);
                                int xInd = i * 3;
                                int yInd = (i * 3) + 1;
                                int zInd = (i * 3) + 2;
                                // Smooth data simple TODO USE LOW PASS INSTEAD
                                if (dataE[xInd].Count > 0)
                                {
                                    JointPreprocessing.LowPassFilter(t,(float)(dataE[xInd][dataE[xInd].Count - 1]), (float)(dataE[yInd][dataE[yInd].Count - 1]), (float)(dataE[zInd][dataE[zInd].Count - 1]), 0.2f);

                                }
                                dataE[xInd].Add(t.X);
                                dataE[yInd].Add(t.Y);
                                dataE[zInd].Add(t.Z);
                                #else
                                vec t = GetNormalisedDirection2d(jointPositions[indicesOfJointPositions[i].Item1].Item2, jointPositions[indicesOfJointPositions[i].Item2].Item2);
                                jointFeatures[i * 2] = t.X;
                                jointFeatures[(i * 2) + 1] = t.Y;
                                #endif
                            }
                        }
                    }                    
                }
            }
            // Print frames
            if (dataE != null && dataE.Count > 0)
                currentBufferFrame.Text = "frames: " + dataE[0].Count.ToString();

            // We need a sensible number of frames before we start attempting to match gestures against remembered sequences
            if (dataE != null && dataE[0].Count > MinimumFrames && _capturing == false && dataG != null && jointPositions.Count == numberOfUniqueJoints)
            {
                // Request lock & Start Quick DTW task. non blocking so just ignores if locked meaning DTW is currently happening
                if (Monitor.TryEnter(DTWQuick.GetDtwQuick))
                {
                    // Instantly release lock so the new task can lock it. This is the only thread that calls PerformFastDTW so no race condition 
                    Monitor.Exit(DTWQuick.GetDtwQuick);
                    DTWQuick.GetDtwQuick.InitDtwQuick(dataE, dataG, jointPositions, indicesOfJointPositions, prevNormCost);
                    DTWQuick.GetDtwQuick.exerciseCompletedEvent = new ExerciseCompletedEvent(OnExerciseCompletedEvent);
                    Task.Run(() => DTWQuick.GetDtwQuick.PerformFastDTW());
                }

            }
            // Ensures that we remember only the last x frames
            if (dataE != null && dataE[0].Count > BufferSize)
            {
                // If we are currently capturing and we reach the maximum buffer size then automatically store
                if (_capturing)
                {
                    btnStore_Click(null, null);
                }
                else
                {
                    // Remove the first frame in the buffer
                    for (int i =0;i< dataE.Count;i++)
                    {
                        dataE[i].RemoveAt(0);
                    }
                }
            }
        }
        void OnExerciseCompletedEvent(Dtw _dtw, bool bExerciseCompleted, double? normCost, double valueEndIndex, int feature)
        {
            if (bExerciseCompleted) // If we completed an exercise
            {
                numOfExercises++;
                // Clear buffers
                //normCosts.Clear();
                prevNormCost = null;
                var dtwPath = _dtw.GetPath();
                int? endIndex = null;
                // Find end index in dataE where valueEndIndex found in DTWQuick to ensure we don't clear data in the buffer of the next exercise
                for (int i = dataE[feature].Count - 1; i > 0; i--) {
                    if (valueEndIndex == dataE[feature][i])
                    {
                        endIndex = i;
                        break;
                    }
                }
                ClearDataEBuffer(endIndex);
                this.Dispatcher.Invoke((Action)(() =>
                {
                    exercises.Text = "Exercise: " + numOfExercises.ToString();
                }));
            }
            else // Else set prevNormCost to normCost
                prevNormCost = normCost;
            if (bPlot)
               plotWin.OnDtwChanged(_dtw);
        }
        // Moving average filter TODO move to more suitable class CONSIDER LOW PASS FILTER AS MORE EFFECIENT
        /*
        public  CalcMovingAverage() {
            decimal buffer[] = new decimal[period];
            decimal output[] = new decimal[data.Length];
            current_index = 0;
            for (int i = 0; i < data.Length; i++)
            {
                buffer[current_index] = data[i] / period;
                decimal ma = 0.0;
                for (int j = 0; j < period; j++)
                {
                    ma += buffer[j];
                }
                output[i] = ma;
                current_index = (current_index + 1) % period;
            }
            return output;
        }*/
        // Find peaks and troughs in a series to minimise the amount of data passed to DTW
        public static List<int> FindPeaks(IList<double> values, int rangeOfPeaks)
        {
            List<int> peaks = new List<int>();
            double current;
            List<double> range;

            int checksOnEachSide = rangeOfPeaks / 2;
            for (int i = values.Count - 1; i > 0; i--)
            {
                current = values[i];
                range = new List<double>(values);

                if (i < (values.Count-checksOnEachSide) && i >= checksOnEachSide)
                {
                    range = range.GetRange(i - checksOnEachSide, rangeOfPeaks);

                    if (range.Count() > 0 && values[i + checksOnEachSide-2] == range.Min())
                    {
                        peaks.Add(i + checksOnEachSide - 2);
                        return peaks;
                    }
                }
            }
            return null;
        }

        // Countdown before recording exercise ground truth
        private void CaptureCountdown(object sender, EventArgs e)
        {
            if (sender == _captureCountdownTimer)
            {
                if (DateTime.Now < _captureCountdown)
                {
                    lblStatus.Text = "Wait " + ((_captureCountdown - DateTime.Now).Seconds + 1) + " seconds";
                }
                else
                {
                    _captureCountdownTimer.Stop();
                    lblStatus.Text = "Recording gesture";
                    StartCapture();
                }
            }
        }

        private void StartCapture()
        {
            btnCapture.IsEnabled = false;
            btnStore.IsEnabled = true;
            // Set the capturing? flag
            _capturing = true;
            ////_captureCountdownTimer.Dispose();
            lblStatus.Text = "Recording gesture";
            // Clear the dataE buffer and start from the beginning
            if (dataE != null)
            {
                ClearDataEBuffer(null);
            }
        }

        private void ClearDataEBuffer(int? endIndex)
        {
            for (int i = 0; i < dataE.Count; i++)
            {
                if (endIndex != null) // If we have an endIndex then delete to there
                    dataE[i].RemoveRange(0, (int)endIndex);
                else // Otherwise clear entire buffer
                    dataE[i].Clear();
            }
        }

        /// <summary>
        /// Handler for recognized speech events.
        /// </summary>
        private void SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            if (bSpeech)
            {
                // Speech utterance confidence below which we treat speech as if it hadn't been heard
                const double ConfidenceThreshold = 0.3;

                if (e.Result.Confidence >= ConfidenceThreshold)
                {
                    switch (e.Result.Semantics.Value.ToString())
                    {
                        case "CAPTURE":
                            btnCapture.PerformClick();
                            break;

                        case "STORE":
                            if (!btnCapture.IsEnabled)
                                btnStore.PerformClick();
                            break;

                        case "SAVE":
                            btnSaveToFile.PerformClick();
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the metadata for the speech recognizer (acoustic model) most suitable to
        /// process audio from Kinect device.
        /// </summary>
        private static RecognizerInfo TryGetKinectRecognizer()
        {
            IEnumerable<RecognizerInfo> recognizers;
            
            // This is required to catch the case when an expected recognizer is not installed.
            // By default - the x86 Speech Runtime is always expected. 
            try
            {
                recognizers = SpeechRecognitionEngine.InstalledRecognizers();
            }
            catch (COMException)
            {
                return null;
            }

            foreach (RecognizerInfo recognizer in recognizers)
            {
                string value;
                recognizer.AdditionalInfo.TryGetValue("Kinect", out value);
                if ("True".Equals(value, StringComparison.OrdinalIgnoreCase) && "en-US".Equals(recognizer.Culture.Name, StringComparison.OrdinalIgnoreCase))
                {
                    return recognizer;
                }
            }

            return null;
        }
        enum CameraMode
        {
            Color,
            Depth,
            Infrared
        }

        System.Numerics.Vector2 GetNormalisedDirection2d(System.Numerics.Vector2 origin, System.Numerics.Vector2 p)
        {
            return System.Numerics.Vector2.Normalize(System.Numerics.Vector2.Subtract(p,origin));
        }
        System.Numerics.Vector3 GetNormalisedDirection3d(System.Numerics.Vector3 origin, System.Numerics.Vector3 p)
        {
            return System.Numerics.Vector3.Normalize(System.Numerics.Vector3.Subtract(p, origin));
        }
    }
}
// Raise click event when speech command is recognised
namespace System.Windows.Controls
{
    public static class MyExt
    {
        public static void PerformClick(this Button btn)
        {
            btn.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
        }
    }
}