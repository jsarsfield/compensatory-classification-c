﻿using NDtw;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DTW_Beta
{
    /// <summary>
    /// Interaction logic for plot.xaml
    /// </summary>
    public partial class PlotWindow : Window
    {
        DtwByVariablePlot dtwXML;

        public PlotWindow()
        {
            InitializeComponent();
            dtwXML = (DtwByVariablePlot)this.FindName("DtwBox");
        }

        public void OnDtwChanged(Dtw v)
        {
            // Draw plot on worker thread
            Dispatcher.BeginInvoke(new Action<Dtw>(dtwXML.OnDtwChanged), DispatcherPriority.Normal,v);
        }

    }
}
