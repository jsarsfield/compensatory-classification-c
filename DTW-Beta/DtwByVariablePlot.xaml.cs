﻿using System;
using System.Windows;
using System.Windows.Controls;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System.Collections.Generic;
using System.Linq;
using NDtw;
using System.ComponentModel;

namespace DTW_Beta
{
    public partial class DtwByVariablePlot : UserControl
    {
        public PlotModel Model { get; set; }
        string YAxisKey = "Direction";
        public BindingList<string> features = new BindingList<string>(); // List of features/seriesVariables/direction vectors
        int feature = 0; // feature to plot

        public DtwByVariablePlot()
        {
            InitializeComponent();
            Model = new PlotModel { Title = "Waiting for input..." };
            Model.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Key = YAxisKey });
            Model.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, Key = "Index" });
            this.DataContext = this;
            this.cbFeature.DataContext = features;  // Bind features to cbFeature
            features.ListChanged += features_ListChanged;
            this.cbFeature.SelectionChanged += cbFeature_SelectedIndexChanged;
        }

        public static DependencyProperty DtwProperty =
            DependencyProperty.Register(
                "Dtw",
                typeof (IDtw),
                typeof(DtwByVariablePlot),
                new FrameworkPropertyMetadata(null, (d, e) => ((DtwByVariablePlot)d).OnDtwChanged(null)));

        public IDtw Dtw
        {
            get { return (IDtw) GetValue(DtwProperty); }
            set { SetValue(DtwProperty, value); }
        }
        void features_ListChanged(object sender, ListChangedEventArgs e)
        {
        }
        private void cbFeature_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (this.cbFeature.SelectedIndex != -1)
                feature = this.cbFeature.SelectedIndex;
        }
        // TODO dropdown list of series variable names for updating feature to plot
        public void OnDtwChanged(Dtw dtw)
        {
            // Add variables name to features list for displaying
            if (features.Count != dtw.SeriesVariables.Length)
            {
                features.Clear();
                for (int i=0;i<dtw.SeriesVariables.Length; i++)
                {
                    features.Add(dtw.SeriesVariables[i].VariableName);
                }
            }
            var dtwPath = dtw.GetPath();
            var xLength = dtw.XLength;
            var yLength = dtw.YLength;
            var cost = dtw.GetCost();
            var costNormalized = dtw.GetCost() / Math.Sqrt(xLength * xLength + yLength * yLength);
            //dtw.SeriesVariables[0].VariableName
            var plotModel = Model;
            plotModel.Series.Clear();
            plotModel.Title = String.Format("dtw norm by length: {0:0.00}, total: {1:0.00}", costNormalized, cost);

            var variableA = dtw.SeriesVariables[feature];
            var variableSeriesT = variableA.GetPreprocessedXSeries();
            var variableB = dtw.SeriesVariables[feature];
            var variableSeriesGT = variableB.GetPreprocessedYSeries();

            var plotSeriesT = new LineSeries { YAxisKey = YAxisKey, Color = OxyPlot.OxyColors.Blue};
            for (int i = 0; i < xLength; i++)
                plotSeriesT.Points.Add(new DataPoint(i, variableSeriesT[i]));

            var plotSeriesGT = new LineSeries { YAxisKey = YAxisKey, Color = OxyPlot.OxyColors.Black };
            for (int i = 0; i < yLength; i++)
                plotSeriesGT.Points.Add(new DataPoint(i, variableSeriesGT[i]));
            var plotSeriesPath = new LineSeries
                                        {    
                                            YAxisKey = YAxisKey,
                                            StrokeThickness = 0.5,
                                            Color = OxyColors.DimGray,
                                        };
            // Draw path between ground truth and stream
            if (dtwPath != null)
            {
                for (int i = 0; i < dtwPath.Length; i++)
                {
                    plotSeriesPath.Points.Add(new DataPoint(dtwPath[i].Item1, variableSeriesT[dtwPath[i].Item1]));
                    plotSeriesPath.Points.Add(new DataPoint(dtwPath[i].Item2, variableSeriesGT[dtwPath[i].Item2]));
                    plotSeriesPath.Points.Add(new DataPoint(double.NaN, double.NaN));
                }
                plotModel.Series.Add(plotSeriesPath);
            }
            plotModel.Series.Add(plotSeriesT);
            plotModel.Series.Add(plotSeriesGT);
                
            /*
            List<int> peaks = MainWindow.FindPeaks(variableSeriesT, 10);
            // Draw troughs (local minima)
            if (peaks != null)
            {
                for (int i = 0; i < peaks.Count; i++)
                {
                    var peakLine = new LineSeries { YAxisKey = YAxisKey, Color = OxyPlot.OxyColors.Red };
                    peakLine.Points.Add(new DataPoint(peaks[i], 0));
                    peakLine.Points.Add(new DataPoint(peaks[i], 1));
                    plotModel.Series.Add(peakLine);
                }
            }*/
            Model = plotModel;
            Model.InvalidatePlot(true);
        }
        /*
        public static List<int> FindPeaks(IList<double> values, int rangeOfPeaks)
        {
            List<int> peaks = new List<int>();
            double current;
            IEnumerable<double> range;
            bool bFoundMax = false;

            int checksOnEachSide = rangeOfPeaks / 2;
            for (int i = values.Count-1; i >= 0; i--)
            {
                current = values[i];
                range = values;

                if (i > checksOnEachSide)
                {
                    range = range.Skip(i - checksOnEachSide);
                }

                range = range.Take(rangeOfPeaks);
                if ((range.Count() > 0) && (current == range.Max()))
                {
                    //peaks.Add(i);
                    bFoundMax = true;
                }
                if (bFoundMax && (range.Count() > 0) && (current == range.Min()))
                {
                    peaks.Add(i);
                    break;
                }
            }

            return peaks;
        }*/
    }
}
